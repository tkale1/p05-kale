package edu.binghamton.cs.hello;

/**
 * Created by pmadden on 2/23/16.
 */

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.content.Context;
import android.util.AttributeSet;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.widget.ImageView;
import android.content.Context;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import static java.util.Random.*;

public class GameView extends View {
    private int mClicks, tapped;
    private float lastX, lastY;
    private Paint mBorderPaint = new Paint();
    private Paint mTextPaint = new Paint();
    private Paint mCirclePaint = new Paint();
    private Timer mTimer;
    private TimerTask mTask;
    private Canvas mCanvas;
    private View self;
    private Boolean circleFinished = true;
    private Random r = new Random();
    private int score = 0;
    int i = r.nextInt((3 - 1) + 1) + 1;
    Bitmap b1 =  BitmapFactory.decodeResource(this.getResources(), R.drawable.f1);
    Bitmap b2 =  BitmapFactory.decodeResource(this.getResources(), R.drawable.f2);
    Bitmap b4 =  BitmapFactory.decodeResource(this.getResources(), R.drawable.f4);
    //i = rand.nextInt((4 - 1) + 1) + 1;

    //private ImageView apple = new ImageView(getContext());

    public void start()
    {
        tapped = 1;
        mBorderPaint.setColor(0xFFCCBB00);
    }

    public GameView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        mClicks = 0;
        tapped = 0;
        lastY = getHeight() / 2;
        lastX = 0;
        mBorderPaint.setColor(0xFFFF0000);
        mTextPaint.setColor(0xFF101010);
        mCirclePaint.setColor(0xFFF00000);
        self = this;


        // mTask = new TimerScheduleFixedRate();
        mTimer = new Timer();
        mTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                // Method
                self.postInvalidate();
                mBorderPaint.setColor(0xFFFF0000);

            }
        }, 0, 20);

        // mTimer.scheduleAtFixedRate();
    }
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int action = event.getAction() & MotionEvent.ACTION_MASK;
        if (action == MotionEvent.ACTION_DOWN) {
            int index = event.getActionIndex();
            //lastX = event.getX(index);
            //lastY = event.getY(index);
            if ((event.getX() > (lastX-50)) && (event.getX() < (lastX+50)) && (event.getY() > (lastY-50)) && (event.getY() < (lastY+50))) {
                i = r.nextInt((3 - 1) + 1) + 1;
                score++;
                circleFinished = true;
            }
            this.postInvalidate();
        }
        return true;
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (circleFinished) {
            lastY = r.nextInt(getHeight() - 100);
            if (lastY < 150)
                lastY = 130;
            if (lastY > getHeight()-250)
                lastY = getHeight()-250;
            lastX = 1;
            circleFinished = false;
        } else {
            lastX+= 10;
        }

        if (lastX >= getWidth() + 50) {
            circleFinished = true;
        }

        //canvas.drawRect(0, 0, getWidth(), getHeight(), mBorderPaint);
        String message = "Score:" + Integer.toString(score);
        mTextPaint.setTextAlign(Paint.Align.CENTER);
        mTextPaint.setTextSize(80);
        mTextPaint.setColor(Color.argb(200, 52, 248, 151));
        canvas.drawText(message, getWidth() - 200, 350, mTextPaint);


        mTextPaint.setTextAlign(Paint.Align.CENTER);
        mTextPaint.setTextSize(150);
        mTextPaint.setColor(Color.argb(55, 52, 208, 151));
        canvas.drawText("FRUIT PRESS", getWidth() / 2, getHeight() - 70, mTextPaint);
        //Rect r1 =new Rect(0, 0, 100, 100);

        if (lastX > 0) {
            canvas.drawCircle(lastX, lastY, 75, mCirclePaint);

            if(i==1)
                canvas.drawBitmap(b1, lastX - 190, lastY - 213, mCirclePaint);

            if(i==2)
                canvas.drawBitmap(b2, lastX-170, lastY-230, mCirclePaint);

            if(i==3)
                canvas.drawBitmap(b4,lastX-90,lastY-110, mCirclePaint);
        }
    }
}
